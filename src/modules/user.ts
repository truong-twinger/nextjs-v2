import { TRole } from './role';

export type TUser = { _id: string, role: TRole };
