import React from 'react';
import { Card, Typography, Col, Row } from 'antd';
import { AiFillHeart, AiOutlineArrowsAlt } from 'react-icons/ai';
import { GiSpookyHouse } from 'react-icons/gi';
import { GrView } from 'react-icons/gr';
import Image from 'next/image';

import style from './style.module.less';

const { Title, Paragraph } = Typography;

const { Meta } = Card;

interface prop {
  company: string;
  money: string;
  location: string;
  project: string;
}
function Box({ company, money, location, project }: prop) {
  return (
    <Row>
      <Col span={24} className={style.box}>
        <Image width={400} height={240} alt='industry' src='/images/image 15.png' />
        <Row className={style.absolute}>
          <Col span={24} className={style.heart}>
            <span className={style.heart}>
              <AiFillHeart />
            </span>
          </Col>
          <Col span={24} className={style.money}>
            <Row style={{ justifyContent: 'space-between' }}>
              <Col span={12} className={style.price}>
                {money}
                <span className={style.span1}>/m</span>
                <span className={style.span2}>2</span>
              </Col>
              <Col span={12} style={{ display: 'flex', justifyContent: 'flex-end', alignItems: 'center' }}>
                <div className={style.watch}>
                  <Image src='/images/Vector (18).png' alt='vector' width={18} height={11} />
                  <p style={{ paddingLeft: '5px' }}>1,238</p>
                </div>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
      <Row style={{ padding: '10px 10px 0 10px' }}>
        <Col span={24}>
          <Paragraph style={{ color: '#C72D1B', fontWeight: '500', margin: '0', paddingBottom: '4px' }}>
            {location}
          </Paragraph>
        </Col>
        <Col span={24}>
          <Title level={5} style={{ fontWeight: '600', fontSize: '18px', margin: '0', paddingBottom: '8px' }}>
            {project}
          </Title>
        </Col>
        <Col span={24}>
          <Row>
            <Col span={8} style={{ color: '#777C84', fontSize: '15px' }}>
              Chủ đầu tư
            </Col>
            <Col span={16} style={{ fontWeight: '400' }}>
              {company}
            </Col>
          </Row>
        </Col>
        <Col span={24} style={{ padding: '4px 0' }}>
          <Row>
            <Col span={8} style={{ color: '#777C84', fontSize: '15px' }}>
              Loại hình
            </Col>
            <Col span={16} style={{ fontWeight: '400' }}>
              : Khu công nghiệp
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Row>
            <div
              style={{
                background: '#F5F5F5',
                padding: '5px',
                display: ' flex ',
                alignItems: 'center',
                marginRight: '10px',
                paddingTop:'10px'
              }}
            >
              <span style={{ paddingRight: '10px' }}>
                <AiOutlineArrowsAlt style={{ color: '#C6CEDB' }} />
              </span>
              <div>
                <span style={{ color: '#51565E' }}>1,783 m</span>
                <span style={{ color: '#51565E' }} className={style.number}>
                  2
                </span>
              </div>
            </div>
            <div style={{ background: '#F5F5F5', padding: '9px', display: ' flex ', alignItems: 'center' }}>
              <span style={{ paddingRight: '10px' }}>
                <GiSpookyHouse style={{ color: '#C6CEDB' }} />
              </span>
              <p style={{ color: '#51565E' }}>Lấp đầy 100%</p>
            </div>
          </Row>
        </Col>
      </Row>
    </Row>
  );
}

export default Box;
