import React from 'react';
import { Card, Row, Col } from 'antd';
import Image from 'next/image';
import { BsFillClockFill } from 'react-icons/bs';

import style from './style.module.less';

interface prop {
  src: string;
  p: string;
}
function CardtCompoent({ src, p }: prop) {
  const truncateString = (str: string, num: number) => {
    if (str?.length > num) {
      return `${str.slice(0, num)} ...`;
    }
    return str;
  };
  return (
    <Card
      className={style.card}
      cover={<Image src={src} width={400} height={241} style={{ objectFit: 'cover', width: '100%' }} alt='banner' />}
    >
      <Row>
        <Col span={24} className={style.time}>
          <BsFillClockFill />
          <span>20/09/2022</span>
        </Col>
        <Col span={24}>
          <p style={{ fontSize: '18px', color: '#252A32', fontWeight: '600',paddingTop:'4px' }}>{truncateString(p,70)}</p>
        </Col>
      </Row>
    </Card>
  );
}

export default CardtCompoent;
