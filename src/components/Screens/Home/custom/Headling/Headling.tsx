import { Button, Col, Row, Typography } from 'antd';
import { IoIosArrowForward } from 'react-icons/io';

import style from './index.module.less';

const { Title, Paragraph } = Typography;
interface prop {
  h1: string;
}
function Headling({ h1 }: prop) {
  return (
    <div className={style.headLing}>
      <span className={style.line} />
      <Row>
        <Col xs={24} sm={24} md={14} lg={14} xl={14}>
          <Title level={3}>{h1}</Title>
        </Col>
        <Col xs={24} sm={24} md={10} lg={10} xl={10} className={style.button}>
          <Button>
            Xem tất cả
            <IoIosArrowForward style={{ fontSize: '12px' }} />
          </Button>
        </Col>
      </Row>
    </div>
  );
}

export default Headling;
