import type { MenuProps } from 'antd';
import { Button, Col, Menu, Row, Select, Typography } from 'antd';
import Image from 'next/image';
import Link from 'next/link';
import { useRef } from 'react';
import { BsSearch } from 'react-icons/bs';
import { FaMoneyBillWave } from 'react-icons/fa';
import { GrNext, GrPrevious } from 'react-icons/gr';
import { ImQuotesLeft, ImQuotesRight } from 'react-icons/im';
import { IoIosArrowForward } from 'react-icons/io';
import { IoLocationSharp } from 'react-icons/io5';
import { MdRefresh } from 'react-icons/md';
import { RiShoppingBagFill } from 'react-icons/ri';
import SwiperCore, { Navigation } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import Layout from '@components/Layouts';
import 'swiper/css';

import Box from './custom/Box/Box';
import CardtCompoent from './custom/Card/CardtCompoent';
import Headling from './custom/Headling/Headling';
import style from './style.module.less';

SwiperCore.use([Navigation]);

const { Title, Paragraph } = Typography;

function HomeScreen() {
  const prevRef = useRef(null);
  const nextRef = useRef(null);
  const items: MenuProps['items'] = [
    {
      label: 'Kiến thức chung',
      key: '1',
    },
    {
      label: 'Hướng dẫn',
      key: '2',
    },
    {
      label: 'Bí kíp',
      key: '3',
    },
    {
      label: 'Tin tức',
      key: '4',
    },
    {
      label: 'Nhận định',
      key: '5',
    },
  ];
  return (
    <Layout title='Home screen'>
      <div className={style.carousel}>
        <div className={style.banner} />
        <div className={style.formSearch}>
          <Row>
            <Col span={24}>
              <div>
                <input placeholder='Nhập từ khoá tìm kiếm' className={style.search} />
                <Button className={style.button4}>
                  <BsSearch />
                </Button>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm={24} md={12} lg={12} xl={7}>
              <Row className={style.provice}>
                <Col span={5}>
                  <span className={style.button5}>
                    <IoLocationSharp />
                  </span>
                </Col>
                <Col span={19}>
                  <Row>
                    <Col span={24}>
                      <Select
                        className={style.select}
                        defaultValue='Tỉnh/ thành phố'
                        style={{ width: 150, border: 'none', outline: 'none', color: '#252A32' }}
                        options={[
                          {
                            value: 'Hà Nội',
                            label: 'Hà Nội',
                          },
                          {
                            value: 'Hải Phòng',
                            label: 'Hải Phòng',
                          },
                          {
                            value: 'Hà Tĩnh',
                            label: 'Hà Tĩnh',
                          },
                        ]}
                      />
                    </Col>
                    <Col span={24}>
                      <p>TP. Hồ Chí Minh</p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm={24} md={12} lg={12} xl={7}>
              <Row className={style.provice}>
                <Col span={5}>
                  <span className={style.button5}>
                    <FaMoneyBillWave />
                  </span>
                </Col>
                <Col span={19}>
                  <Row>
                    <Col span={24}>
                      <Select
                        className={style.select}
                        defaultValue='Loại giao dịch'
                        style={{ width: 135, border: 'none', outline: 'none', color: '#252A32' }}
                        options={[
                          {
                            value: 'Tiền mặt',
                            label: 'Tiền mặt',
                          },
                          {
                            value: 'Tiền mặt',
                            label: 'Tiền mặt',
                          },
                          {
                            value: 'Tiền mặt',
                            label: 'Tiền mặt',
                          },
                        ]}
                      />
                    </Col>
                    <Col span={24}>
                      <p>Chuyển nhượng</p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm={24} md={12} lg={12} xl={7}>
              <Row className={style.provice}>
                <Col span={5}>
                  <span className={style.button5}>
                    <RiShoppingBagFill />
                  </span>
                </Col>
                <Col span={19}>
                  <Row>
                    <Col span={24}>
                      <Select
                        className={style.select}
                        defaultValue='Loại hình'
                        style={{ width: 110, border: 'none', outline: 'none' }}
                        options={[
                          {
                            value: 'Nhượng quyền',
                            label: 'Nhượng quyền',
                          },
                          {
                            value: 'Nhượng quyền',
                            label: 'Nhượng quyền',
                          },
                          {
                            value: 'Nhượng quyền',
                            label: 'Nhượng quyền',
                          },
                        ]}
                      />
                    </Col>
                    <Col span={24}>
                      <p>Khu công nghiệp</p>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col sm={24} md={12} lg={12} xl={3} style={{ justifyContent: 'flex-end' }}>
              <span
                style={{
                  width: '44px',
                  height: '44px',
                  borderRadius: '50%',
                  lineHeight: '50px',
                  textAlign: 'center',
                  fontSize: '18px',
                  backgroundColor: '#C72D1B',
                  color: '#fff',
                  cursor: 'pointer',
                }}
              >
                <MdRefresh />
              </span>
            </Col>
          </Row>
        </div>
      </div>
      <div className={style.find}>
        <div className={style.background}>
          <div className={style.blur} />
          <div className='container' style={{ paddingTop: '104px', paddingBottom: '64px' }}>
            <div className={style.invest}>
              <Row>
                <Col span={24} style={{ display: 'flex', justifyContent: 'center', marginBottom: '48px' }}>
                  <ImQuotesLeft className={style.quote} />
                  <Title level={3}>
                    KHÁM PHÁ
                    <span style={{ color: '#C72D1B', padding: '0 7px' }}>ĐỊA ĐIỂM ĐẦU TƯ</span>
                    CỦA BẠN
                  </Title>
                  <ImQuotesRight className={style.quote} />
                </Col>
                <Col span={24}>
                  <Row gutter={[40, 0]}>
                    <Col sm={24} md={24} lg={8} xl={8}>
                      <Row className={style.col}>
                        <Col span={24}>
                          <Image
                            src='/images/Vector (17).png'
                            width={56}
                            height={56}
                            alt='house'
                            className={style.house}
                          />
                        </Col>
                        <Col span={24}>
                          <Title level={5}>Tìm kiếm dự án đầu tư</Title>
                        </Col>
                        <Col span={24}>
                          <Paragraph className={style.paragrap}>
                            Bảng hàng trực tuyến của hơn 483 dự án duy nhất chỉ có trên 2KCN.vn
                          </Paragraph>
                        </Col>
                        <Col span={24}>
                          <Link
                            style={{
                              color: '#C72D1B',
                              display: 'flex',
                              alignItems: 'center',
                              maxWidth: '90%',
                              fontSize: '16px',
                              fontWeight: '500',
                            }}
                            href='/'
                          >
                            Tìm kiếm
                            <IoIosArrowForward style={{ fontSize: '12px' }} />
                          </Link>
                        </Col>
                      </Row>
                    </Col>
                    <Col sm={24} md={24} lg={8} xl={8}>
                      <Row className={style.col}>
                        <Col span={24}>
                          <Image src='/images/Frame.png' width={56} height={56} alt='house' className={style.house} />
                        </Col>
                        <Col span={24}>
                          <Title level={5}>Tìm kiếm dự án đầu tư</Title>
                        </Col>
                        <Col span={24}>
                          <Paragraph className={style.paragrap}>
                            Bảng hàng trực tuyến của hơn 483 dự án duy nhất chỉ có trên 2KCN.vn
                          </Paragraph>
                        </Col>
                        <Col span={24}>
                          <Link
                            style={{
                              color: '#C72D1B',
                              display: 'flex',
                              alignItems: 'center',
                              maxWidth: '90%',
                              fontSize: '16px',
                              fontWeight: '500',
                            }}
                            href='/'
                          >
                            Tìm kiếm
                            <IoIosArrowForward style={{ fontSize: '12px' }} />
                          </Link>
                        </Col>
                      </Row>
                    </Col>
                    <Col sm={24} md={24} lg={8} xl={8}>
                      <Row className={style.col}>
                        <Col span={24}>
                          <Image
                            src='/images/Frame (1).png'
                            width={56}
                            height={56}
                            alt='house'
                            className={style.house}
                          />
                        </Col>
                        <Col span={24}>
                          <Title level={5}>Tìm kiếm dự án đầu tư</Title>
                        </Col>
                        <Col span={24}>
                          <Paragraph className={style.paragrap}>
                            Bảng hàng trực tuyến của hơn 483 dự án duy nhất chỉ có trên 2KCN.vn
                          </Paragraph>
                        </Col>
                        <Col span={24}>
                          <Link
                            style={{
                              color: '#C72D1B',
                              display: 'flex',
                              alignItems: 'center',
                              maxWidth: '90%',
                              fontSize: '16px',
                              fontWeight: '500',
                            }}
                            href='/'
                          >
                            Tìm kiếm
                            <IoIosArrowForward style={{ fontSize: '12px' }} />
                          </Link>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        </div>
      </div>
      <div className='container'>
        <div className={style.industry}>
          <Headling h1='DỰ ÁN KHU CÔNG NGHIỆP' />
          <Row gutter={[40, 40]}>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
            <Col sm={24} md={12} lg={12} xl={8}>
              <Box
                company=': Công ty CP Long Hậu'
                project='Dự án KCN Long Hậu'
                location='Ninh Thạnh, Tây Ninh'
                money='124,000'
              />
            </Col>
          </Row>
        </div>
      </div>
      <div className='container'>
        <Row style={{ padding: '80px 5.55555556% 0 5.55555556%' }}>
          <Col span={24}>
            <Image src='/images/GDN.png' width={1280} height={312} alt='banner' className={style.ad} />
          </Col>
        </Row>
      </div>
      <div className='container'>
        <div className={style.knowleage}>
          <Headling h1='Kiến thức đầu tư' />
          <Row style={{ marginBottom: '45px' }}>
            <Col span={24}>
              <Menu defaultSelectedKeys={['1']} mode='horizontal' items={items} />
            </Col>
          </Row>
          <Row>
            <Col span={24} className={style.slider}>
              <Swiper
                spaceBetween={40}
                slidesPerView={3}
                onInit={(swiper) => {
                  swiper.params.navigation.prevEl = prevRef.current;
                  swiper.params.navigation.nextEl = nextRef.current;
                  swiper.navigation.init();
                  swiper.navigation.update();
                }}
                breakpoints={{
                  320: {
                    slidesPerView: 1,
                  },
                  600: {
                    slidesPerView: 2,
                  },
                  991: {
                    slidesPerView: 3,
                  },
                }}
              >
                <SwiperSlide>
                  <CardtCompoent src='/images/image 7.png' p='Tân trang nhà cũ, làm mới không gian sống' />
                </SwiperSlide>
                <SwiperSlide>
                  <CardtCompoent
                    src='/images/image 12.png'
                    p='Đầu tư đất nông nghiệp, lợi nhuận cao nhưng cần thận trọng điều gì?'
                  />
                </SwiperSlide>
                <SwiperSlide>
                  <CardtCompoent
                    src='/images/image 13.png'
                    p='Đại dịch tái định hình khái niệm sức khỏe, bất động sản trị liệu lên ngôi'
                  />
                </SwiperSlide>
                <SwiperSlide>
                  <CardtCompoent src='/images/image 7.png' p='Tân trang nhà cũ, làm mới không gian sống' />
                </SwiperSlide>
                <SwiperSlide>
                  <CardtCompoent
                    src='/images/image 13.png'
                    p='Đại dịch tái định hình khái niệm sức khỏe, bất động sản trị liệu lên ngôi'
                  />
                </SwiperSlide>
              </Swiper>
              <Button className={style.prev} ref={prevRef}>
                <GrPrevious />
              </Button>
              <Button className={style.next} ref={nextRef}>
                <GrNext />
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    </Layout>
  );
}

export default HomeScreen;
