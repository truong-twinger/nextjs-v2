import React, { HTMLAttributes, PropsWithChildren } from 'react';
import { ToastContainer } from 'react-toastify';

import Footer from './Footer';
import Head, { THead } from './Head';
import Header from './Header';

type PageProps = PropsWithChildren<THead> & HTMLAttributes<HTMLDivElement>;
function Layout(props: PageProps) {
  const { children } = props;
  return (
    <>
      <Head {...props} />
      <Header />
      {children}
      <Footer />
      <ToastContainer
        position='top-right'
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme='light'
      />
    </>
  );
}

export default Layout;
