import Image from 'next/image';
import Link from 'next/link';
import React, { useState } from 'react';
import { FiMenu } from 'react-icons/fi';
import { Button, Drawer, Row, Col } from 'antd';

import style from './style.module.less';

function Header() {
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  return (
    <div className='container'>
      <div className={style.header}>
        <Row>
          <Col span={24}>
            <Row className={style.flexMenu}>
              <Col span={2}>
                <div className={style.hidden}>
                  <Link href='/'>
                    <Image
                      alt='logo'
                      className={style.logo}
                      width={110}
                      height={38}
                      src='/images/LOGO-2KCN-SVG-01 1 (1).png'
                    />
                  </Link>
                  <Button type='primary' onClick={showDrawer} className={style.menu}>
                    <FiMenu style={{ fontSize: '20px' }} />
                  </Button>
                  {/* <Button onClick={toggle} className={style.menu}>
                    {isOpen ? <AiOutlineClose style={{ fontSize: '20px' }} /> : <FiMenu style={{ fontSize: '20px' }} />}
                  </Button> */}
                </div>
              </Col>
              <Col span={19} className={style.teXt}>
                <Row style={{ alignItems: 'center', justifyContent: 'flex-end' }}>
                  <Col span={17}>
                    <ul className={style.ul}>
                      <Link href='/'>
                        <li className={style.li}>Mua/Bán</li>
                      </Link>
                      <Link href='/'>
                        <li className={style.li}>Cho thuê</li>
                      </Link>
                      <Link href='/'>
                        <li className={style.li}>Chuyển nhượng</li>
                      </Link>
                      <Link href='/'>
                        <li className={style.li}>M&A</li>
                      </Link>
                      <Link href='/'>
                        <li className={style.li}>Dự án KCN</li>
                      </Link>
                      <Link href='/'>
                        <li className={style.li}>Tin tức</li>
                      </Link>
                    </ul>
                  </Col>
                  <Col span={6}>
                    <Row gutter={[8, 0]}>
                      <Col span={12}>
                        <Button className={style.button1}>Đăng tin</Button>
                      </Col>
                      <Col span={12}>
                        <Button className={style.button2}>Ký gửi</Button>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col span={3} className={style.maxWidthButton}>
                <Button className={style.button3}>Đăng nhập / Đăng ký</Button>
              </Col>
            </Row>
          </Col>
        </Row>
        <Drawer
          title={
            <Image alt='logo' className={style.logo} width={110} height={38} src='/images/LOGO-2KCN-SVG-01 1 (1).png' />
          }
          placement='right'
          onClose={onClose}
          open={open}
        >
          <ul>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>Mua/Bán</li>
            </Link>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>Cho thuê</li>
            </Link>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>
                Chuyển nhượng
              </li>
            </Link>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>M&A</li>
            </Link>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>Dự án KCN</li>
            </Link>
            <Link href='/' style={{ display: 'flex', justifyContent: 'center' }}>
              <li style={{ listStyle: 'none', color: '#252A32', padding: '15px 0', fontSize: '16px' }}>Tin tức</li>
            </Link>
          </ul>
          <Row style={{ paddingTop: '15px' }}>
            <Col span={24}>
              <Row>
                <Col span={12} style={{ display: 'flex', justifyContent: 'center' }}>
                  <Button
                    style={{
                      width: '115px',
                      height: '38px',
                      background: '#F59E19',
                      color: '#fff',
                      borderRadius: '5px',
                    }}
                  >
                    Đăng tin
                  </Button>
                </Col>
                <Col span={12} style={{ display: 'flex', justifyContent: 'center' }}>
                  <Button style={{ width: '115px', height: '38px', color: '#252A32', borderRadius: '5px' }}>
                    Ký gửi
                  </Button>
                </Col>
              </Row>
            </Col>
            <Col span={24} style={{ display: 'flex', justifyContent: 'center' }}>
              <Button style={{ width: '170px', height: '38px', marginTop: '10px', borderRadius: '5px' }}>
                Đăng nhập / Đăng ký
              </Button>
            </Col>
          </Row>
        </Drawer>
      </div>
    </div>
  );
}

export default Header;
