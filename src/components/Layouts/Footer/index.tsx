import React from 'react';
import Image from 'next/image';
import { Col, Row, Typography, Input } from 'antd';
import Link from 'next/link';
import { MdLocationOn } from 'react-icons/md';
import { BsFillTelephoneFill, BsTwitter } from 'react-icons/bs';
import { FaFacebookF } from 'react-icons/fa';
import { AiFillInstagram } from 'react-icons/ai';

import style from './Foote.module.less';

const { Title, Paragraph } = Typography;
const { Search } = Input;

function Footer() {
  return (
    <div style={{ backgroundColor: '#000' }}>
      <div className='container'>
        <Row className={style.footer}>
          <Col xs={24} sm={24} md={24} lg={9} xl={9} style={{ paddingRight: '10.41666667%' }}>
            <Row className={style.address}>
              <Col span={24}>
                <Link href='/'>
                  <Image alt='logo' width={110} height={38} src='/images/LOGO-2KCN-SVG-01 1 (1).png' />
                </Link>
              </Col>
              <Col span={24}>
                <Title
                  level={5}
                  style={{ color: '#fff', textTransform: 'uppercase', paddingBottom: '16px', paddingTop: '48px' }}
                >
                  Công ty cổ phần 2KCN Việt Nam
                </Title>
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={24} style={{ display: 'flex' }}>
                    <span style={{ marginRight: '10px', fontSize: '18px', marginTop: '5px' }}>
                      <MdLocationOn />
                    </span>
                    <Paragraph style={{ color: 'rgba(255, 255, 255, 0.8)', fontSize: '14px' }}>
                      5th floor, Song Da Building, 18 Pham Hung, My Dinh, Hanoi
                    </Paragraph>
                  </Col>
                </Row>
              </Col>
              <Col span={24}>
                <Row>
                  <Col span={24} style={{ display: 'flex' }}>
                    <span style={{ marginRight: '10px', fontSize: '17px', marginTop: '5px' }}>
                      <BsFillTelephoneFill />
                    </span>
                    <Paragraph style={{ color: 'rgba(255, 255, 255, 0.8)', fontSize: '14px' }}>0243 984 4568</Paragraph>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
          <Col xs={24} sm={24} md={12} lg={8} xl={8}>
            <Row>
              <Col span={12} className={style.tutorial}>
                <Col span={24}>
                  <Title level={5} style={{ color: '#fff', textTransform: 'uppercase', paddingBottom: '16px' }}>
                    Hướng dẫn
                  </Title>
                </Col>
                <Col span={24}>
                  <Link href='/'>Báo giá & hỗ trợ</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Câu hỏi thường gặp</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Thông báo</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Liên hệ</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Sitemap</Link>
                </Col>
              </Col>
              <Col span={12} className={style.tutorial}>
                <Col span={24}>
                  <Title level={5} style={{ color: '#fff', textTransform: 'uppercase', paddingBottom: '16px' }}>
                    Quy định
                  </Title>
                </Col>
                <Col span={24}>
                  <Link href='/'>Quy định đăng tin</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Quy chế hoạt động</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Chính sách bảo mật</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Chính sách bảo mật</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Giải quyết khiếu nại</Link>
                </Col>
                <Col span={24}>
                  <Link href='/'>Góp ý báo lỗi</Link>
                </Col>
              </Col>
            </Row>
          </Col>
          <Col xs={24} sm={24} md={12} lg={6} xl={6} className={style.search}>
            <Row>
              <Col span={24}>
                <Title level={5} style={{ color: '#fff', textTransform: 'uppercase', paddingBottom: '16px' }}>
                  Đăng ký nhận tin
                </Title>
              </Col>
              <Col span={24}>
                {/* <Search placeholder='Nhập địa chỉ email' className={style.search} enterButton='Gửi' size='large' /> */}
                <form>
                  <div className={style.search} style={{width:'100%',height:'42px',display:'flex',border:'1px solid #51565E',borderRadius:'5px'}}>
                    <input style={{width:'100%',border:'none',paddingLeft:'20px',borderStartStartRadius:"5px",borderBottomLeftRadius:'5px'}} placeholder='Nhập địa chỉ email'></input>
                    <button style={{width:'80px',border:'none',color:'#fff',borderEndEndRadius:"5px",borderTopRightRadius:'5px'}}>Gửi</button>
                  </div>
                </form>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Title
                  level={5}
                  style={{ color: '#fff', textTransform: 'uppercase', paddingBottom: '16px', paddingTop: '56px' }}
                >
                  Kết nối với chúng tôi
                </Title>
              </Col>
              <Col span={24} className={style.social}>
                <Link href='/'>
                  <FaFacebookF />
                </Link>
                <Link href='/'>
                  <AiFillInstagram />
                </Link>
                <Link href='/'>
                  <BsTwitter />
                </Link>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className={style.footerBottom}>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Paragraph style={{color:'#777C84'}}>Giấy phép Mạng xã hội số 12/GP-BTTTT do Bộ thông tin và Truyền thông cấp ngày 11/01/2023.</Paragraph>
          </Col>
          <Col xs={24} sm={24} md={24} lg={12} xl={12}>
            <Paragraph className={style.paragraph1}>Copyright © 2022 2KCN. All rights reserved.</Paragraph>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Footer;
